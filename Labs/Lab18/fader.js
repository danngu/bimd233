$(document).ready(function () {
  $('li').css('margin', '10px');
  $('li').attr('id', 'uw');

  $('#p1 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeOut(2000, function () {
      console.log("fadeout complete!")
    });
  });

  $('#p2 li').fadeIn(4000, function () {
      console.log("fadein complete!")
  });

  $('#p3 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeTo(2000, 0.4, function () {
      console.log("fadeto complete!")
    });
  });

  $('#p4 li').click(function () {
    console.log("$(this):" + $(this));
    $(this).fadeToggle(2000, function () {
      console.log("fadetoggle complete!")
    });
  });

});