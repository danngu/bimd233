$(document).ready(function() {
  $("li").css("id", "uw");
  const states = ["default", "gather"];
  var state = states[0];
  var words = new Array();
  var ndx = 0;

  $("ul").on("mouseover", "li", function() {
    console.log("x:" + $(this).text());
    $(this).attr("id", "uw");
  });

  $("ul").on("mouseleave", "li", function() {
    $(this).attr("id", "uw-gold");
  });

  // reset button click
  $("button").on("click", function(e) {
    state = "default";
    str = "";
    $('li').remove();
  });

  var str = "";

  // keypress
  $("input").on("keypress", function(e) {
    var code = e.which;
    var char = String.fromCharCode(code);
    str += char;
    console.log('key:' + code + '\tstate:' + state + '\nchar:' + char + '\tstr:' + str);
    switch (state) {

      case "gather":
        str = char;
        state = "default";
        break;

      default:
        if (code == 13) {
          $('ul').append($('<li>').text(str));
          state = "gather";
          $(this).val('');
          e.preventDefault();
        }
        break;
    }
  });
});
