
var geo = [calcCircleGeometries(Math.floor(Math.random() * 20)), calcCircleGeometries(Math.floor(Math.random() * 20)), calcCircleGeometries(Math.floor(Math.random() * 20))];



function table() {
    var i, j, id;
    for (i = 0; i < geo.length; i++) {
        for (j = 0; j < geo[0].length; j++) {
            id = i.toString() + j.toString();
            document.getElementById(id).innerHTML = geo[i][j];
        }
    }
}

function calcCircleGeometries(radius) { 
    const pi = Math.PI; 
    var area = pi * radius * radius; 
    var circumference = 2 * pi * radius; 
    var diameter = 2 * radius; 
    var geometries = [radius, area, circumference, diameter]; 
    return geometries; 
}