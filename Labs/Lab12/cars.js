var toyo = ['Toyota', 'Corolla L', '$18,500', 2017];
var lexus = ['Lexus', 'RX 350', '$43,220', 2017];
var merc = ['Mercedes-Benz', 'C300 Sedan', '$39,500', 2017];
var audi = ['Audi', 'A4 2.0T Ultra Premium', '$34,900', 2017];
var honda = ['Honda', 'Civic Sedan', '$18,740', 2017];

var cars = [toyo, lexus, merc, audi, honda];

function table() {
    var i, j, id;
    for (i = 0; i < cars.length; i++) {
        for (j = 0; j < cars[0].length; j++) {
            id = i.toString() + j.toString();
            document.getElementById(id).innerHTML = cars[i][j];
        }
    }
}