var schools =   [{ School: "Stanford", Conference: "7-2", Overall: "9-5", id: "su", img: "<img class='img' src='su.png'>"},
                { School: "Washington", Conference: "7-2", Overall: "10-3", id: "uw", img: "<img class='img' src='uw.png'>"},
                { School: "Washington State", Conference: "6-3", Overall: "9-4", id: "wsu", img: "<img class='img' src='wsu.png'>"},
                { School: "Oregon", Conference: "4-5", Overall: "7-6", id: "ou", img: "<img class='img' src='ou.png'>"},
                { School: "California", Conference: "2-7", Overall: "5-7", id: "cal", img: "<img class='img' src='cal.png'>"},
                { School: "Oregon State", Conference: "0-9", Overall: "1-11", id: "osu", img: "<img class='img' src='osu.png'>"}];

var el = document.getElementById('one');

function myFunction(item, index) {
    el.innerHTML += "<div class='row' id='" + item.id + "'><div class='col-sm-4 college'>" + item.img + item.School + "</div><div class='col-sm-4 conference'>" + item.Conference + "</div><div class='col-sm-4 overall'>" + item.Overall + "</div></div>";
    el = el.nextElementSibling;
}