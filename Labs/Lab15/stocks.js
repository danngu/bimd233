var stocks =  [{ company: "Microsoft", markCap: "$381.7 B", sales: "$86.8 B", profit: "1%", employee: 128000}, 
                { company: "Symetra Financial", markCap: "$2.7 B", sales: "$2.2 B", profit: "15.3%", employee: 1400},
                { company: "Micron Technology", markCap: "$37.6 B", sales: "$16.4 B", profit: "155.9%", employee: 30400},
                { company: "F5 Networks", markCap: "$9.5 B", sales: "$1.7 B", profit: "12.2%", employee: 3834},
                { company: "Expedia", markCap: "$10.8 B", sales: "$5.8 B", profit: "71%", employee: 18210},
                { company: "Nautilus", markCap: "$476 M", sales: "$274.4 M", profit: "-60.8%", employee: 340 },
                { company: "Heritage Financial", markCap: "$531 M", sales: "$137.6 M", profit: "119.5%", employee: 748 },
                { company: "Cascade Microtech", markCap: "$239 M", sales: "$136 M", profit: "-26%", employee: 449 },
                { company: "Nike", markCap: "$83.1 B", sales: "$27.8 B", profit: "8.9%", employee: 56500 },
                { company: "Alaska Air Group", markCap: "$7.9 B", sales: "$5.4 B", profit: "19.1%", employee: 13952 }];

var el = document.getElementById('table_data');
el.innerHTML = "<tr> <th colspan='5' id='head'>NW Stock Companies</th></tr><tr><th>Company</th> <th>Market Cap</th> <th>Sales</th> <th>Profit</th> <th>Employees</th>";


function myFunction(item, index) { 
    console.log(item, index)
    if (index % 2 == 0) {
        el.innerHTML += "<tr class='even'><td>" + item.company + "</td><td>" + item.markCap + "</td><td>" + item.sales + "</td><td>" + item.profit + "</td><td>" + item.employee + "</td></tr>";
    } else {
        el.innerHTML += "<tr><td>" + item.company + "</td><td>" + item.markCap + "</td><td>" + item.sales + "</td><td>" + item.profit + "</td><td>" + item.employee + "</td></tr>";
    }
}