// Shorthand for $( document ).ready()
$(function() {
  // weather update button click
  $('button').one('click', function(e) {
    
    $('ul').empty();
    
    $.ajax({
      url: "http://api.wunderground.com/api/e9b40de286d87ba2/geolookup/conditions/q/WA/Bothell.json",
      dataType: "jsonp",
      success: function(parsed_json) {
        var city = parsed_json['location']['city'];
        var state = parsed_json['location']['state'];
        var temp_f = parsed_json['current_observation']['temp_f'];
        var rh = parsed_json['current_observation']['relative_humidity'];
        var wind = parsed_json['current_observation']['wind_string'];
        var weather = parsed_json['current_observation']['weather'];
        var icon = parsed_json['current_observation']['icon_url'];
        var str = "<li> Location: " + city + ", " + state + "</li>";
        console.log(temp_f + ", " + rh + ", " + wind + ", " + weather);

        // Update list items with data from above...
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
        str = "<li> Weather: " + weather + "<img src='" + icon + "'>" + "</li>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
        str = "<li> Current temperature: " + temp_f + "</li>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
        str = "<li> Relative Humidity: " + rh + "</li>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');
        str = "<li> Wind: " + wind + "</li>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');

      }
    });
  });
});
